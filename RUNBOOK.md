# Common maintenance tasks

### Problem

* an upstream [security-report-schemas](https://gitlab.com/gitlab-org/security-products/security-report-schemas) pipeline failed to trigger the release pipeline
* you want to add, remove or deprecate support for report schema versions
* you need to release a new version of the gem without altering version ranges,
  because for example:
  * an existing gem release has a bug, and the bugfix release needs to cover the
  same version range.
  * there were breaking changes to the gem's public API that must be released
  for the currently supported version range.

### Solution

1. Open and merge an MR targeting the default branch which may:
    * change the [`supported_versions`](../supported_versions) file to set the
      report schema version range that the release should include.
    * change the `Gitlab::SecurityReportSchemas::Version::GEM_VERSION` constant
      to set the MAJOR.MINOR.PATCH version components of the resulting release.
2. Run a new pipeline for the default branch and set the `MANUAL_RELEASE` CI
   variable.
3. Trigger the manual `manual-release` job in the resulting pipeline.

## Find the commit SHA for a RubyGem version

Before a rubygems.org release is created, a git tag referencing the full
v-prefixed release version is pushed, for example `v0.1.0.min15.0.0.max15.0.1`.
