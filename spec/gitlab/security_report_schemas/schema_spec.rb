# frozen_string_literal: true

RSpec.describe Gitlab::SecurityReportSchemas::Schema do
  let(:report_type) { "sast" }

  subject(:schema) { described_class.new(report_type, "v0.2.0") }

  it { is_expected.to delegate_method(:supported?).to(:schema_ver) }
  it { is_expected.to delegate_method(:deprecated?).to(:schema_ver) }
  it { is_expected.to delegate_method(:fallback?).to(:schema_ver) }
  it { is_expected.to delegate_method(:version).to(:schema_ver).with_prefix }

  describe "#validate" do
    subject { schema.validate(data).to_a }

    context "when the data is invalid" do
      let(:data) { { "version" => "0.2.0", "name" => "fo" } }

      it { is_expected.to match([a_hash_including("data_pointer" => "/name", "type" => "minLength", "data" => "fo")]) }
    end

    context "when the data is valid" do
      let(:data) { { "version" => "0.2.0", "name" => "foo" } }

      it { is_expected.to be_empty }
    end

    context "when the `report_type` is `api_fuzzing`" do
      let(:report_type) { "api_fuzzing" }
      let(:data) { { "version" => "0.2.0", "name" => "foo" } }

      it { is_expected.to be_empty }
    end
  end
end
