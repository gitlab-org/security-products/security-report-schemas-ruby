# frozen_string_literal: true

require "gitlab/security_report_schemas/cli/validator"

RSpec.describe Gitlab::SecurityReportSchemas::CLI::Validator do
  describe "#run" do
    let(:service_object) { described_class.new(argv) }
    let(:version_range) do
      [Gitlab::SecurityReportSchemas::Version.min_schema, Gitlab::SecurityReportSchemas::Version.max_schema].map(&:to_s)
    end

    subject(:validate) { service_object.run }

    context "when an unrecognized option is provided" do
      let(:argv) { ["-a", "something"] }
      let(:exit_prompt) do
        <<~MSG
          invalid option: -a
          SecurityReportSchemas #{Gitlab::SecurityReportSchemas::Version}.
          Supported schema versions: \e[32m#{version_range.inspect}\e[0m

          Usage: security-report-schemas REPORT_FILE_PATH [options]
              -r, --report_type=REPORT_TYPE    Override the report type
              -w, --warnings                   Prints the warning messages
        MSG
      end

      it "prompts the usage along with invalid option name and exits the program" do
        expect { validate }.to raise_error(SystemExit)
                           .and output(exit_prompt).to_stdout
      end
    end

    context "when the `file_path` is not provided" do
      let(:argv) { [] }
      let(:exit_prompt) do
        <<~MSG
          SecurityReportSchemas #{Gitlab::SecurityReportSchemas::Version}.
          Supported schema versions: \e[32m#{version_range.inspect}\e[0m

          Usage: security-report-schemas REPORT_FILE_PATH [options]
              -r, --report_type=REPORT_TYPE    Override the report type
              -w, --warnings                   Prints the warning messages
        MSG
      end

      it "prompts the usage and exits the program" do
        expect { validate }.to raise_error(SystemExit)
                           .and output(exit_prompt).to_stdout
      end
    end

    context "when the `file_path` is provided" do
      let(:args) { [] }
      let(:argv) { [file_path, *args] }

      context "when the `report_type` is not provided" do
        context "when the `report_type` can not be inferred from the report content" do
          let(:file_path) { file_fixture("reports", "report_without_scan.json") }
          let(:exit_prompt) { "Can not find report type! Consider providing the report type.\n" }

          it "prompts the error and exits the program" do
            expect { validate }.to raise_error(SystemExit)
                               .and output(exit_prompt).to_stdout
          end
        end

        context "when the `report_type` can be inferred from the report content" do
          let(:file_path) { file_fixture("reports", "report_with_scan.json") }
          let(:valid?) { false }
          let(:mock_validator) do
            instance_double(
              Gitlab::SecurityReportSchemas::Validator,
              valid?: valid?, errors: ["error message"], warnings: ["warning message"], schema_ver_version: "0.0.0"
            )
          end
          let(:validation_message) { "Validating sast v15.0.0 against schema v0.0.0\n" }

          before do
            allow(Gitlab::SecurityReportSchemas::Validator).to receive(:new).and_return(mock_validator)
          end

          it "instantiates an instance of validator with correct arguments" do
            validate

            expect(Gitlab::SecurityReportSchemas::Validator).to have_received(:new).with(an_instance_of(Hash),
                                                                                         "sast", "15.0.0")
          end

          context "when the report is invalid" do
            context "when the warnings flag is not set" do
              it "prints only the error messages" do
                expect { validate }.to output(
                  "#{validation_message}\e[31mContent is invalid\e[0m\n* error message\n"
                ).to_stdout
              end
            end

            context "when the warnings flag is set" do
              let(:args) { ["-w"] }

              it "prints the error messages along with the warnings" do
                expect { validate }.to output(
                  "#{validation_message}\e[31mContent is invalid\e[0m\n" \
                  "* error message\n" \
                  "\e[33m* warning message\e[0m\n"
                ).to_stdout
              end
            end
          end

          context "when the report is valid" do
            let(:valid?) { true }

            context "when the warnings flag is not set" do
              it "prints the content is valid message" do
                expect { validate }.to output("#{validation_message}\e[32mContent is valid\e[0m\n").to_stdout
              end
            end

            context "when the warnings flag is set" do
              let(:args) { ["-w"] }

              it "prints the content is valid message along with the warnings" do
                expect { validate }.to output(
                  "#{validation_message}\e[32mContent is valid\e[0m\n\e[33m* warning message\e[0m\n"
                ).to_stdout
              end
            end
          end
        end
      end

      context "when the `report_type` is provided" do
        let(:file_path) { file_fixture("reports", "report_with_scan.json") }
        let(:args) { ["-r", "dependency_scanning"] }
        let(:mock_validator) do
          instance_double(
            Gitlab::SecurityReportSchemas::Validator,
            valid?: true, errors: [], warnings: [], schema_ver_version: "0.0.0"
          )
        end

        before do
          allow(Gitlab::SecurityReportSchemas::Validator).to receive(:new).and_return(mock_validator)
        end

        it "uses the given report type and instantiates an instance of validator with correct arguments" do
          validate

          expect(Gitlab::SecurityReportSchemas::Validator)
            .to have_received(:new).with(an_instance_of(Hash), "dependency_scanning", "15.0.0")
        end
      end
    end
  end
end
