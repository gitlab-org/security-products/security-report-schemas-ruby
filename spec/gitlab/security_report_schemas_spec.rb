# frozen_string_literal: true

RSpec.describe Gitlab::SecurityReportSchemas do
  describe ".configure" do
    subject(:configure) { described_class.configure { |configuration| configuration } }

    it "calls the given block with the configuration" do
      expect(configure).to eq(described_class.configuration)
    end
  end

  describe ".supported_versions" do
    subject { described_class.supported_versions.map(&:to_s) }

    it { is_expected.to match_array(["0.1.0", "0.2.0"]) }
  end

  describe ".maintained_versions" do
    subject { described_class.maintained_versions.map(&:to_s) }

    before do
      described_class.configure do |config|
        config.deprecated_versions = ["0.1.0"]
      end
    end

    it { is_expected.to match_array(["0.2.0"]) }
  end

  describe ".deprecated_versions" do
    subject { described_class.deprecated_versions.map(&:to_s) }

    before do
      described_class.configure do |config|
        config.deprecated_versions = ["0.1.0"]
      end
    end

    it { is_expected.to match_array(["0.1.0"]) }
  end

  describe ".schemas_path" do
    let(:path) { "foo" }

    subject { described_class.schemas_path }

    around do |spec|
      previous_schemas_path = described_class.schemas_path
      described_class.configure { |config| config.schemas_path = path }

      spec.run
    ensure
      described_class.configure { |config| config.schemas_path = previous_schemas_path }
    end

    it { is_expected.to eq(path) }
  end
end
